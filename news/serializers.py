from rest_framework import serializers

from news.models import Article, NewsSite


class ArticleSerializer(serializers.ModelSerializer):
    class Meta:
        model = Article
        fields = "__all__"


class NewsSiteSerializer(serializers.ModelSerializer):
    site = ArticleSerializer(many=True, read_only=True)

    class Meta:
        model = NewsSite
        fields = ("id", "name", "site")
