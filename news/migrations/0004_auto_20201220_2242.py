# Generated by Django 3.1.4 on 2020-12-20 20:42

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("news", "0003_auto_20201217_0152"),
    ]

    operations = [
        migrations.AlterField(
            model_name="article",
            name="title",
            field=models.CharField(max_length=300),
        ),
    ]
