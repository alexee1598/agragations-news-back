from django.urls import path

from news import views

urlpatterns = [
    path("", views.schema_board_view, name="index-view"),
]
