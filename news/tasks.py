from agregationNewsBack.celery import app
from news.helper import get_newsone_article, get_sharij_news


@app.task(name="politics_news")
def scrap_politics_news() -> None:
    get_sharij_news()
    get_newsone_article()
