from django.db.models import Prefetch
from rest_framework.decorators import api_view
from rest_framework.response import Response

from news.models import Article, NewsSite
from news.serializers import NewsSiteSerializer


@api_view(["GET"])
def schema_board_view(request):
    queryset = NewsSite.objects.prefetch_related(
        Prefetch("site", queryset=Article.objects.filter(archived=False)))
    return Response({
        "response": NewsSiteSerializer(queryset, many=True).data,
    })
