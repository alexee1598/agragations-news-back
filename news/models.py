from django.db import models


class NewsSite(models.Model):
    POLITICS = "POLITICS"
    SPORT = "SPORT"
    TECH = "TECH"
    ENTERTAINMENT = "ENTERTAINMENT"
    LIFESTYLE = "LIFESTYLE"
    BUSINESS = "BUSINESS"

    TOPIC_CHOICES = (
        (POLITICS, POLITICS),
        (SPORT, SPORT),
        (TECH, TECH),
        (ENTERTAINMENT, ENTERTAINMENT),
        (LIFESTYLE, LIFESTYLE),
        (BUSINESS, BUSINESS),
    )

    name = models.CharField(max_length=100)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    url_address = models.URLField(max_length=400)
    topic = models.CharField(max_length=20,
                             choices=TOPIC_CHOICES,
                             default=POLITICS)


class Article(models.Model):
    news_site = models.ForeignKey(NewsSite,
                                  on_delete=models.CASCADE,
                                  related_name="site")
    url_address = models.URLField(max_length=400)
    title = models.CharField(max_length=300)
    description = models.TextField(max_length=300)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    archived = models.BooleanField(default=False)
