import datetime
import json
import re

import requests
from bs4 import BeautifulSoup

from news.models import Article, NewsSite
from news.site_const import *


def get_sharij_news() -> None:
    response = requests.get(SHARIJ_URL, headers=USER_AGENT)
    soup = BeautifulSoup(response.text, "html.parser")
    articles = soup.find(class_="featured_mobile news d-lg-none").find_all(
        "a", class_="title", href=True)
    site_id = NewsSite.objects.get(name="Sharij").pk

    for art in articles[:-1]:
        url_address = art["href"]
        title = art.text

        article = requests.get(url_address, headers=USER_AGENT)
        article_soup = BeautifulSoup(article.text, "html.parser")
        article_soup = article_soup.find("script", type="application/ld+json")
        data = json.loads(
            re.search(r"({(\s*.)+)</", str(article_soup)).group(1))

        description = ""
        created = datetime.datetime.now()
        try:
            description = data["@graph"][2]["description"]
            created = data["@graph"][2]["datePublished"]
        except Exception as e:
            pass
        _save_article(
            title=title,
            url_address=url_address,
            news_site=site_id,
            description=description,
            created=created,
        )
    _set_archive_article(site_id=site_id)


def get_newsone_article() -> None:
    response = requests.get(NEWSONE_URL, headers=USER_AGENT)
    soup = BeautifulSoup(response.text, "html.parser")
    articles = soup.find(class_="main-slider").find_all(
        "figure", class_="main-slider-item")
    site_id = NewsSite.objects.get(name="NewsOne").pk

    for art in articles:
        url_address = NEWSONE_URL[:-1] + art.find("a")["href"]
        title = art.find("figcaption").text

        article = requests.get(f"{url_address}", headers=USER_AGENT)
        article_soup = BeautifulSoup(article.text, "html.parser")
        article_soup = article_soup.find("script", type="application/ld+json")
        data = json.loads(
            re.search(r"({(\s*.)+)</", str(article_soup)).group(1))

        description = ""
        created = datetime.datetime.now()
        try:
            description = data["description"]
            created = data["datePublished"]
        except Exception as e:
            pass
        _save_article(
            title=title,
            url_address=url_address,
            news_site=site_id,
            description=description,
            created=created,
        )
    _set_archive_article(site_id=site_id)


def _save_article(
    title: str,
    url_address: str,
    news_site: int,
    description: str,
    created: datetime = None,
) -> None:
    art = Article.objects.filter(title=title).first()
    if art:
        art.updated = datetime.datetime.now()
        art.save()
        return
    a = Article()
    a.url_address = url_address
    a.news_site = NewsSite.objects.get(pk=news_site)
    a.title = title
    a.description = description
    a.created = created
    a.save()


def _set_archive_article(site_id: int) -> None:
    articles = Article.objects.filter(news_site__pk=site_id,
                                      archived=False).order_by("-updated")
    for a in articles[5:]:
        a.archived = True
        a.save()
